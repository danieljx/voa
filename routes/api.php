<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'LoginController@login');
//Route::group(['middleware' => ['jwt.verify.api']], function() {
    Route::get('/logout', 'LoginController@logout');
    Route::get('/list', 'OAController@list');
    Route::get('/view/{oa}/{path?}', 'OAController@voa')->where('path', '(.*)');
//});
