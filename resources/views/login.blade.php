<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pegui View OA</title>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="/css/login.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="card card-container">
                <img id="profile-img" class="profile-img-card" src="/img/logo.png" />
                <p id="profile-name" class="profile-name-card"></p>
                {{ Form::open(array('url' => 'api/login', 'class' => 'form-signin')) }}
                    <span id="reauth-email" class="reauth-email"></span>
                    <input type="text" id="token" name="token" class="form-control" value="7E3718B1662F2E46878F9A8C4D330A1CC7D88C88E1D64023414A2329E54878D8" placeholder="Token" required autofocus>
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">
                        Autorizar
                    </button>
                {{ Form::close() }}
                <footer class="footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="copyright-text">
                                    <p>
                                        <a href="http://www.catedra.edu.co/" target="_blank">
                                            Powered by Catedr@ ©
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="/js/login.js" ></script>
    </body>
</html>