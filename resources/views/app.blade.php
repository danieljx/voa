<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pegui View OA</title>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="root"></div>
        <footer class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="copyright-text">
                            <p>
                                <a href="http://www.catedra.edu.co/" target="_blank">
                                    Powered by Catedr@ ©
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script>
            var API_URL = '{{url('/api')}}';
        </script>
        <script src="{{mix('js/app.js')}}" ></script>
    </body>
</html>