import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap';

class ListItem extends Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this);
        this.state = {
            show: false
        }
    }
    
    onClose() {
        this.setState({ show: false });
    }
    
    render() {
        return (
            <tr id={this.props.oa.id}>
                <th className="number" scope="row">{this.props.ix}</th>
                <td className="category">
                    <h4 className="h4-filter">
                        <span className={"label label-" + this.props.oa.category.color}>{this.props.oa.category.name}</span>
                    </h4>
                </td>
                <td className="name">{this.props.oa.name}</td>
                <td className="size hidden-xs">{this.props.oa.size}</td>
                <td className="action">
                    <div role="group">
                        <a href={this.props.oa.url} rel="noreferrer" target="_blank" className={"btn btn-success" + (this.props.oa.uncompress ? ' disabled' : '')} title="Ver">
                            <i className="glyphicon glyphicon-eye-open"></i>
                        </a>
                        <div className="hide">
                            <Modal
                                show={this.state.show}
                                onHide={this.onClose}
                                bsSize="large"
                                container={this}
                                aria-labelledby="contained-modal-title"
                                dialogClassName="oa-modal"
                            >
                                <Modal.Header closeButton>
                                    <Modal.Title>{this.props.oa.name}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <div className="embed-responsive embed-responsive-16by9">
                                        <iframe src={this.props.oa.url} className="embed-responsive-item"></iframe>
                                    </div>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={this.onClose}>Close</Button>
                                </Modal.Footer>
                            </Modal>
                        </div>
                    </div>
                </td>
            </tr>
        )
    }
}

export default ListItem;