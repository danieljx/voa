import React from 'react'
import { Table } from 'react-bootstrap';
import ListItem from './ListItem'

const List = ({ oas, onView, onUncompress, onDownload }) => {
	const Items = oas.map((oa, ix) => {
  		return (
                <ListItem
                    ix={(ix+1)}
                    oa={oa}
                    key={oa.id}
                    onView={onView}
                    onUncompress={onUncompress}
                    onDownload={onDownload}
                />
        )
  	});

    return (
        <Table id="listOas" responsive striped bordered condensed hover>
            <thead>
                <tr>
                    <th className="number">#</th>
                    <th className="category">Categoria</th>
                    <th className="name">Nombre</th>
                    <th className="size hidden-xs">Size</th>
                    <th className="action">
                        <i className="glyphicon glyphicon-asterisk"></i>
                    </th>
                </tr> 
            </thead>
            <tbody>
                {Items}
            </tbody>
        </Table>
    );
}

export default List;