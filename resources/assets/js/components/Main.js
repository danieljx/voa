import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import List from './List';
import Pagination from "./Pagination";

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: true,
            oas: [],
            pageSize: 9,
            pageOfItems: [],
            selectCategory: null,
            categorys: null,
            filter: '',
            filteredOas: [],
            currentOas: [],
            currentPage: 0,
            totalPages: 0,
            isLoading: false,
            error: null
        }

        this.fetchList = this.fetchList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onFilterOas = this.onFilterOas.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onShow = this.onShow.bind(this);
        this.onView = this.onView.bind(this);
        this.onCategory = this.onCategory.bind(this);
        this.onPageSize = this.onPageSize.bind(this);
    }

    fetchList() {
        this.setState({ isLoading: true });
        fetch(API_URL + '/list').then((response) => {
            response.json().then((jsonResponse) => {
                this.setState({ oas: jsonResponse.oas });
                this.setState({ filteredOas: jsonResponse.oas });
                this.setState({ categorys: jsonResponse.categorys });
                this.setState({ selectCategory: jsonResponse.selectCategory });
                this.setState({ isLoading: false });
            })
            .catch(error => this.setState({ error, isLoading: false }));
        });
    }

    onPageSize(e) {
        const pageSize = e.target.value;
        this.setState({ pageSize: pageSize });
    }

    handleChange(e) {
        const onFilterOas = this.onFilterOas;
        const str = e.target.value;
        this.setState({ filter: str }, function() {
            onFilterOas();
        });
    }
    
    onCategory(folder) {
        const onFilterOas = this.onFilterOas;
        const category = this.state.categorys.filter((cat) => {
            return cat.folder == folder
        });
        this.setState({ selectCategory: category[0] }, function() {
            onFilterOas();
        });
    }

    onFilterOas() {
        const { selectCategory, filter } = this.state;
        const filtered = this.state.oas.filter((oa) => {
            const name = oa.name.toLowerCase();
            const basename = oa.basename.toLowerCase();
            const category = oa.category.folder.toLowerCase();
            const searchCOndition   = (!filter || filter == '' || name.indexOf(filter.toLowerCase()) !== -1 || basename.indexOf(filter.toLowerCase()) !== -1)
            const categoryCondition = (selectCategory.folder == 'all' || category == selectCategory.folder.toLowerCase());
            return (categoryCondition && searchCOndition);
        });
        this.setState({ filteredOas: filtered });
    }

    onChangePage(pageOfItems, page, totalPages) {
        this.setState({
            pageOfItems: pageOfItems, 
            currentPage: (page?page:1),
            totalPages: totalPages 
        });
    }    

    onClose() {
        this.setState({ show: false });
    }

    onShow() {
        this.setState({ show: true });
    }

    onView(oa) {
        onShow();
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-default navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="/">
                                <img src="img/logo.png" alt="Dispute Bills" />
                            </a>
                        </div>
                        <div id="navbar1" className="navbar-collapse collapse">
                            <form className="navbar-form navbar-left" role="search">
                                <div className="form-group">
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Search"
                                            disabled={!this.state.oas.length}
                                            value={this.state.filter}
                                            onChange={this.handleChange}
                                        />
                                        {
                                            (this.state.selectCategory ? (
                                                <span className="input-group-addon">
                                                    <h4 className="h4-filter">
                                                        <span className={"label label-" + this.state.selectCategory.color}>{this.state.selectCategory.name}</span>
                                                    </h4>
                                                </span>
                                            ) : (null))
                                        }
                                        
                                        <div className="input-group-btn">
                                            <button
                                                disabled={!this.state.oas.length}
                                                type="button" 
                                                className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span className="caret"></span>
                                                <span className="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul className="dropdown-menu dropdown-menu-right">
                                            {
                                                (this.state.categorys ?
                                                    this.state.categorys.map((cat, ix) => {
                                                        return (
                                                            <li key={ix}>
                                                                <a href="#" onClick={() => this.onCategory(cat.folder)}>
                                                                    <h4 className="h4-filter">
                                                                        <span className={"label label-" + cat.color}>{cat.name}</span>
                                                                    </h4>
                                                                </a>
                                                            </li>
                                                        )
                                                    })
                                                : (null))
                                            }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </nav>
                <div className="container">
                    <div className="row">
                        <div className="col col-xs-6 col-md-5">
                            <div className="row">
                                <div className="col col-xs-12 col-lg-5">
                                    <h2 className="titleTotal">
                                        <strong className="text-secondary">{this.state.filteredOas.length}</strong> OA's
                                    </h2>
                                </div>
                                <div className="col col-xs-12 col-lg-7">
                                    <h2 className="current-page text-secondary">
                                        Page <strong>{this.state.currentPage}</strong> /{" "}
                                        <strong>{this.state.totalPages}</strong>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div className="col col-xs-12 col-md-5">
                            <Pagination
                                items={this.state.filteredOas}
                                pageSize={this.state.pageSize}
                                onChangePage={this.onChangePage}
                            />
                        </div>
                        <div className="col col-xs-12 col-md-2">
                            {
                                this.state.filteredOas.length ? (
                                    <div className="row">
                                        <label className="labelCantidad control-label col col-sm-6">Cantidad: </label>
                                        <div className="col col-sm-6">
                                            <select className="selectCantidad form-control" value={this.state.pageSize} onChange={this.onPageSize}>
                                                <option value="5">5</option>
                                                <option value="9">9</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value={this.state.filteredOas.length}>Todos</option>
                                            </select>
                                        </div>
                                    </div>
                                ) : (null)
                            }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel panel-default panel-table">
                                <div className="panel-heading">
                                    <div className="row">
                                        <div className="col col-xs-6">
                                            <h3 className="panel-title">Lista OA's</h3>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-body">
                                    {
                                        this.state.isLoading ? (
                                            <div className="box-loading">
                                                <div className="loading">
                                                    <div className="loader"></div>
                                                    <div className="antiLoader"></div>
                                                </div>
                                            </div>
                                    ) : ( <List
                                            oas={this.state.pageOfItems}
                                            onView={this.onView}
                                            onUncompress={this.onUncompress}
                                            onDownload={this.onDownload}
                                        /> )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.fetchList();
    }
}

export default Main;

if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}