<?php

namespace App\Providers;

use Tymon\JWTAuth\Providers\Auth\AuthInterface;

class CatedraAuthenticationProvider implements AuthInterface {
    
    public function byCredentials(array $credentials = []) {
        return $credentials['token'] == env('AUTH_TOKEN');
    }

    public function byId($id) {
        // maybe throw an expection?
    }

    public function user() {
        // you will have to implement this maybe.
    }
}
