<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;
use League\Flysystem\Util\MimeType;

class OAController extends Controller {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $Adapter;
    protected $Filesystem;
    protected $url;
    protected $oas;
    protected $compress;
    protected $uncompress;
    protected $folderZip;

    public function __construct(Request $request) {
        $this->request      = $request;
        $this->url          = env("DATA_URL");
        $this->oas          = env("DATA_OAS");
        $this->compress     = env("DATA_COMPRESS");
        $this->uncompress   = env("DATA_UNCOMPRESS");
        $this->Adapter      = new Local(storage_path(env("DATA_ROOT")));
        $this->Filesystem   = new Filesystem($this->Adapter);
    }

    public function list() {
        $oas        = [];
        $categorys  = [];
        $folderZip  = $this->Filesystem->listContents($this->oas, false);
        $count = 0;
        foreach ($folderZip as $folder) {
            $mimetype   = $this->Filesystem->getMimetype($folder['path']);
            if($mimetype == 'directory') {
                $category = [];
                if($this->Filesystem->has($folder['path'] . DIRECTORY_SEPARATOR . 'data.json')) {
                    $category = json_decode($this->Filesystem->read($folder['path'] . DIRECTORY_SEPARATOR . 'data.json'), true);
                    $categorys[] = $category;
                }
                $sort = array();
                foreach ($categorys as $key => $row) {
                    $sort[$key] = $row['sort'];
                }
                array_multisort($sort, SORT_ASC, $categorys);
                $dataOas  = $this->Filesystem->listContents($folder['path'], false);
                foreach ($dataOas as $oa) {
                    if($oa['type'] == 'dir') {
                        $dataItem = $oa;
                        $this->cleanDataResponse($dataItem);
                        $dataItem['id'] = date("YmdHis").$count;
                        $dataItem['size'] = $this->getSizeOas($dataItem['path']);
                        $dataItem['category'] = $category;
                        $dataItem['metadata'] = $this->getMetaData($dataItem['path']);
                        $dataItem['url'] = $this->url . "/" . str_replace('oas/', '', $dataItem['path']) . '/' . (isset($dataItem['metadata']['href'])?$dataItem['metadata']['href']:$dataItem['filename'] . '.html');
                        if(isset($dataItem['metadata']['identifier'])) {
                            $dataItem['name'] = $dataItem['metadata']['identifier'];
                        } else {
                            $dataItem['name'] = $dataItem['filename'];
                        }
                        $oas[] = $dataItem;
                        $count++;
                    }
                }
            }
        }
        $selectCategory = [
            "name"      => 'Todos',
            "folder"    => "all",
            "color"     => 'default'
        ];
        array_push($categorys, $selectCategory);
        $data = [
            'categorys' => $categorys,
            'selectCategory' => $selectCategory,
            'oas' => $oas
        ];
        return response()->json($data);
    }

    private function getSizeOas($oaPath) {
        $directory = storage_path(env("DATA_ROOT")) . DIRECTORY_SEPARATOR . $oaPath;
        return $this->convertToReadableSize($this->getDirSize($directory));
    }

    private function getDirSize($directory) {
        $size = 0;
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file) {
            $size += $file->getSize();
        }
        return $size;
    }

    private function getMetaData($oaPath) {
        if($this->Filesystem->has($oaPath . DIRECTORY_SEPARATOR . 'imsmanifest.xml')) {
            $xml = simplexml_load_string($this->Filesystem->read($oaPath . DIRECTORY_SEPARATOR . 'imsmanifest.xml'));
            $array = json_decode(json_encode((array) $xml), true);
            $metaArray = [];
            if(isset($array['resources']['resource']["@attributes"])) {
                $metaArray = $array['resources']['resource']["@attributes"];
            } else {
                if(isset($array['resources']['resource'][0])) {
                    $metaArray = $array['resources']['resource'][0]["@attributes"];
                }
            }
            return $metaArray;
        } else {
            return [];
        }
    }

    public function voa($oa, $path = "") {
        $file   = $oa . (!empty($path)?DIRECTORY_SEPARATOR . $path:'');
        $exists = $this->Filesystem->has($file);
        if($exists) {
            $mimetype   = $this->Filesystem->getMimetype($file);
            if($mimetype == 'directory') {
                $index = $file . DIRECTORY_SEPARATOR . $oa . '.html';
                if($this->Filesystem->has($index)) {
                    return redirect($this->request->path() . '/' . $oa . '.html');
                } else {
                    $index = "";
                    $metadata = $this->getMetaData($file);
                    if(isset($metadata['href'])) {
                        $index = $metadata['href'];
                    } else {
                        $data = [];
                        $fileList = $this->Filesystem->listContents($file, false);
                        foreach ($fileList as $file) {
                            if(isset($file['extension']) && $file['extension'] == 'html' && $file['basename'] != '_fallback.html') {
                                $this->cleanDataResponse($file);
                                $data[] = $file;
                            }
                        }
                        if(count($data) < 2) {
                            $index = $data[0]['filename'] . '.html';
                        }
                    }
                    if(!empty($index)) {
                        return redirect($this->request->path() . '/' . $index);
                    } else {
                        return response()->json($data);
                    }
                }
            } else {
                $this->fileResponseView($file);
            }
        } else {
            throw new Exception('RESOURCE_NOT_OPEN');
        }
    }

    private function fileResponseView($fileName) {
        $path = storage_path(env("DATA_ROOT")) . DIRECTORY_SEPARATOR . $fileName;
        if (is_file($path)) {
            $file = fopen($path, "rb");
            $size = filesize($path);
            $content = fread($file, $size);
            fclose($file);
            $response = new StreamedResponse(function () use ($content) {
                $out = fopen('php://output', 'w');
                fputs($out, $content);
                fclose($out);
              }, 200 , [
                'Content-Type' => MimeType::detectByFilename($fileName),
                'Content-Disposition' => "filename='" . $fileName . "'",
                'Cache-control' => 'private',
                'Content-length' => $this->Filesystem->getSize($fileName)
            ]);
            $response->send();
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
    }

    private function cleanDataResponse(&$data) {
        unset($data['dirname']);
        unset($data['timestamp']);
    }

    private function convertToReadableSize($size) {
        $base = log($size) / log(1024);
        $suffix = array("", "KB", "MB", "GB", "TB");
        $f_base = floor($base);
        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    }
}
